# Laravel Take Home Exercise
Copyrights (c) [5.5 Degrees](http://www.5andhalf.com)

## HOW TO START?
1. Click "Run Project"
2. Browse to "http://charlie-balot-batman5andhalf.c9users.io/pet"
2. Read Test Document
3. ...
100. START CODING.

## INFO
You can test / develop enpoints using POSTMAN  
You can access your database through [PHPMYADMIN](http://charlie-balot-batman5andhalf.c9users.io/phpmyadmin)  
    - Username: batman5andhalf  
    - Password: (BLANK / NO PASSWORD)  
	
## BitBucket Repository
https://bitbucket.org/cbalot/5andhalflaravelexam.git

### Framework Documentation
Documentation for the framework can be found on the
[Laravel website](http://laravel.com/docs).
