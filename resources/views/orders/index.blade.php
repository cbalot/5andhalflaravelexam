<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width" />
<title>John Doe Pet Shop</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap
/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>John Doe Pet Shop</h1>
 <a href="{{url('/books/create')}}" class="btn btn-success">Add order</a>
 <hr>
 <table class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Id</th>
         <th>PetId</th>
         <th>Quantity</th>
         <th>Ship Date</th>
         <th>Status</th>
         <th>Complete</th>
         <th><center>Actions</center></th>
     </tr>
     </thead>
     <tbody>
     @foreach($orders as $order)
         <tr>
             <td>{{ $order->id }}</td>
             <td>{{ $order->petId }}</td>
             <td>{{ $order->quantity }}</td>
             <td>{{ $order->shipDate }}</td>
             <td>{{ $order->status }}</td>
             <td>{{ $order->complete }}</td>
             <td>
                  <center>
                    <a href="#" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-fullscreen"></span> View
                    </a>
                    <a href="#" class="btn btn-sm btn-info">
                       <span class="glyphicon glyphicon-pencil"></span> Edit
                    </a>
                    <a class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')" 
                    href="#">
                        <span class="glyphicon glyphicon-trash"></span> Delete
                    </a>
                 </center>
              </td>
         </tr>
     @endforeach

     </tbody>

 </table>
    </div>
</body>
</html>