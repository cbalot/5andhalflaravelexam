<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width" />
<title>John Doe Pet Shop</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap
/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="col-md-12">
          <h1>Create Pet Record</h1>
          <hr>
          <form class="form-horizontal" method="post" action="store">
               {{ csrf_field() }}
            <div class="form-group">
              <label class="control-label col-sm-2">Category:</label>
              <div class="col-sm-6">
                <select class="form-control" id="category" name="category">
                    <option select></option>
                     @foreach($categories as $category)
                         <option value="{{$category->id}}">{{$category->name}}</option>
                     @endforeach
                </select>
              </div>
            </div><!--End of category div-->
            <div class="form-group">
              <label class="control-label col-sm-2"><span style="color:red;">*</span> Name:</label>
              <div class="col-sm-6"> 
                <input type="text" class="form-control" id="name" name="name" required>
              </div>
            </div><!--End of Name div-->
            <div class="form-group">
              <label class="control-label col-sm-2"><span style="color:red;">*</span>Photo:</label>
              <div class="col-sm-6"> 
                <input type="file" class="form-control" id="photoUrls" name="photoUrls" required>
              </div>
            </div><!--End of Photo div-->
            <div class="form-group">
              <label class="control-label col-sm-2">Tags:</label>
              <div class="col-sm-6">
                <select class="form-control" id="tags" name="tags" multiple>
                     @foreach($tags as $tag)
                         <option value="{{$tag->name}}">{{$tag->name}}</option>
                     @endforeach
                </select>
              </div>
            </div><!--End og Tags div-->
            <div class="form-group"> 
              <div class="col-sm-offset-2 col-sm-10">
                <a href="{{url('pet/')}}" class="btn btn-sm btn-default"> 
                  Cancel
                </a>
                <button type="submit" class="btn btn-success btn-sm">Save</button>
              </div>
            </div><!--End of submit button -->
          </form> <!--End of form tag-->
        </div>
    </div>
</body><!--End of body-->
</html>