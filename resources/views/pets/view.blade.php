<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width" />
<title>John Doe Pet Shop</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap
/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>View Pet record</h1>
        <hr>
        <div class="row">
          <div class="col-md-2">
            <label class="control-label col-sm-2">Category:</label>
          </div>
          <div class="col-md-6">
            <span class="control-label col-sm-2">
               @foreach($categories as $category)
                  @if ($category->id == $pet->category)
                      {{$category->name}}
                  @endif
               @endforeach
            </span>
          </div>
        </div><!--End of .row-->
        <div class="row">
          <div class="col-md-2">
            <label class="control-label col-sm-2">Name:</label>
          </div>
          <div class="col-md-6">
            <span class="control-label col-sm-2">{{$pet->name}}</span>
          </div>
        </div><!--End of .row-->
        <div class="row">
          <div class="col-md-2">
            <label class="control-label col-sm-2">Photo:</label>
          </div>
          <div class="col-md-6">
            <span class="control-label col-sm-2">
              <img src="{{$pet->photoUrls}}" height="100" width="100" class="img-rounded">
            </span>
          </div>
        </div><!--End of .row-->
        <div class="row">
          <div class="col-md-2">
            <label class="control-label col-sm-2">Tags:</label>
          </div>
          <div class="col-md-6">
            <span class="control-label col-sm-2">{{$pet->tags}}</span>
          </div>
        </div><!--End of .row-->
        <br/>
        <div class="form-group"> 
           <a href="{{url('pet/')}}" class="btn btn-sm btn-default"> 
             <span class="glyphicon glyphicon-arrow-left"></span> Back to Pet Records
            </a>
            <a href="{{url('pet/edit',$pet->id)}}" class="btn btn-sm btn-info">
               <span class="glyphicon glyphicon-pencil"></span> Edit
            </a>
        </div><!--End of submit button -->
    </div><!--End of .container-->
</body><!--End of body-->
</html>