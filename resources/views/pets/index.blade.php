<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width" />
<title>John Doe Pet Shop</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap
/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>John Doe Pet Shop</h1>
        <hr>
        @if(Session::has('add_message'))
            <div class="alert alert-success">
                {{ Session::get('add_message') }}
            </div>
        @elseif(Session::has('update_message'))
            <div class="alert alert-info">
                {{ Session::get('update_message') }}
            </div>
        @elseif(Session::has('delete_message'))
            <div class="alert alert-danger">
                {{ Session::get('delete_message') }}
            </div>
        @endif
         <a href="{{url('/pet/create')}}" class="btn btn-success">
             <span class="glyphicon glyphicon-plus"></span> Add Pet
        </a>
         <hr>
         <form class="form-horizontal" method="post" action="{{action('PetController@search')}}">
             {{ csrf_field() }}
             <div class="row">
                 <div class="col-md-6 col-sm-6">
                    @if(Session::has('search_message'))
                        {{ Session::get('search_message') }}
                    @endif
                 </div>
                 <div class="col-md-6 col-sm-6 pull-right">
                    <div class="input-group">
                      <input type="text" class="form-control" id="searchTag" name="searchTag" autocomplete="off" placeholder="Search tags" aria-label="Search tags">
                      <span class="input-group-btn">
                        <button class="btn btn-secondary" type="submit">Search</button>
                      </span>
                    </div>
                 </div>
             </div>
         </form>
         <br/>
         <div class="table-responsive">
             <table class="table table-striped table-bordered table-hover">
                 <thead>
                 <tr class="bg-info">
                     <th>Id</th>
                     <th>Category</th>
                     <th>Name</th>
                     <th>Photo</th>
                     <th>Tags</th>
                     <th>Status</th>
                     <th><center>Actions</center></th>
                 </tr>
                 </thead>
                 <tbody>
                 @foreach($pets as $pet)
                     <tr>
                         <td>{{ $pet->id }}</td>
                         <td>
                             @foreach($categories as $category)
                                  @if($pet->category == $category->id)
                                     {{$category->name}}
                                 @endif
                             @endforeach
                            
                         </td>
                         <td>{{ $pet->name }}</td>
                         <td>
                            <center>
                                <img src="{{$pet->photoUrls}}" height="100" width="100" class="img-rounded">
                            </center>
                         </td>
                         <td><span class="label label-info">{{$pet->tags}}</span></td>
                         <td>{{ $pet->status }}</td>
                         <td>
                             <center>
                                <a href="{{url('pet/view',$pet->id)}}" class="btn btn-sm btn-primary">
                                    <span class="glyphicon glyphicon-fullscreen"></span> View
                                </a>
                                <a href="{{url('pet/edit',$pet->id)}}" class="btn btn-sm btn-info">
                                   <span class="glyphicon glyphicon-pencil"></span> Edit
                                </a>
                                <a class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')" 
                                href="{{url('pet/destroy', $pet->id)}}">
                                    <span class="glyphicon glyphicon-trash"></span> Delete
                                </a>
                             </center>
                          </td>
                     </tr>
                 @endforeach
            
                 </tbody>
            
             </table>
         </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script>
        $(".delete").on("submit", function(){
            return confirm("Do you want to delete this item?");
        });
    </script>
</body>

</html>