<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Set the index page to the pet page.
Route::get('/', function () {
    return view('welcome');
});

//URL for Pets
Route::get('/pet', 'PetController@index');
Route::get('/pet/create', 'PetController@create');
Route::post('/pet/store', 'PetController@store');
Route::get('/pet/view/{id}', 'PetController@view');
Route::get('/pet/edit/{id}', 'PetController@edit');
Route::post('/pet/update/{id}', 'PetController@update');
Route::get('/pet/destroy/{id}', 'PetController@destroy');

//URL to search pets by tags
Route::post('/pet/search', 'PetController@search');


//URL for Orders
Route::get('/order', 'OrderController@index');