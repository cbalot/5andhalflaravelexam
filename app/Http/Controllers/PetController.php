<?php

namespace App\Http\Controllers;

use Request;
use Session;
use Builder;
use App\Pet;
use App\Category;
use App\Tags;
use App\Http\Requests;

class PetController extends Controller
{
    /**
    * Display a listing of the resource of pets.
    *
    * @return Response
    */
   public function index()
   {
       $pets = Pet::all();
       $categories = Category::all();
       return view('pets.index', compact(['pets', 'categories']));
   }
   /**
    * Show the form for creating a new resource.
    * 
    * @return pets/create view and data of tags and categories
    */
   public function create()
   {
      $tags = Tags::all();
      $categories = Category::all();
      return view('pets.create', compact(['tags', 'categories']));
   }
   
   /**
     * Store a newly created pet record.
     *
     * @return Response to the pets page.
     */
    public function store(Request $request)
    {
        //setting up the filepath of the image
        $path = public_path() . "/img/";
        $img = $request::get('photoUrls');
        $data = base64_decode($img);
        $success = file_put_contents($path."".$img, $data);//move the image/picture into the declated path.
        
        //get all the tags selected.
        $tags = $request::get('tags'); 
        //$tags= implode("|",$tags);
        
        $values = array(
           'category' => $request::get('category'), 
           'name' => $request::get('name'),
           'photoUrls' => $path."".$img,
           'status' => "available",
           'tags' => $tags
           );
           
        Pet::create($values);
        Session::flash('add_message', 'Pet record successfully added!');
        return redirect('/pet');
    }
   
   /**
    * Display the specified pet record.
    *
    * @param  int  $id
    * @return Response
    */
   public function view($id)
   {
      $pet = Pet::find($id);
      $categories = Category::all();
      return view('pets.view', compact(['pet', 'categories']));
   }

   /**
    * Show the form for editing the specified pet record.
    *
    * @param  int  $id
    * @return Response
    */
   public function edit($id)
   {
      $pet = Pet::find($id);
      $tags = Tags::all();
      $categories = Category::all();
      return view('pets.edit', compact(['pet', 'tags', 'categories']));
   }
   
   /**
    * Update the specified pet record to the DB.
    *
    * @param  int  $id
    * @return Response
    */
   public function update(Request $request, $id)
   {
       //setting up the filepath of the image
        if($request::get('photoUrls') != ""){
            $path = public_path() . "/img/";
            $img = $request::get('photoUrls');
            $data = base64_decode($img);
            $success = file_put_contents($path."".$img, $data);//move the image/picture into the declated path.
        }
        
        //get all the tags selected.
        $tags = $request::get('tags'); 
        //$tags= implode("|",$tags);
       
        Session::flash('update_message', 'Pet record successfully updated!');
        
        $crud = Pet::find($id);
        $crud->category = $request::get('category');
        $crud->name = $request::get('name');
        if($request::get('photoUrls') != ""){
            $crud->photoUrls = $path."".$img;
        }
        $crud->tags = $tags;
        $crud->save();
        
        return redirect('/pet');
   }
   
   /**
    * Remove the specified pet id from DB.
    * 
    * @param  int  $id
    * @return Response
    */
   public function destroy($id)
   {
        Pet::find($id)->delete();
        Session::flash('delete_message', 'Pet record successfully deleted!');
        return redirect('/pet');
   }
   
   /**
    * Search pet reacords by tag.
    * 
    * @param  Request  $request
    * @return Response
    */
   public function search(Request $request)
   {
        $searchTag = $request::get('searchTag');
        
        if($searchTag != ""){
            $pets = Pet::where('tags', 'like', '%'.$searchTag.'%' )->get();
            $searchCount = Pet::where('tags', 'like', '%'.$searchTag.'%')->count();
        
            if($searchCount > 0){
                Session::flash('search_message', $searchCount." found result.");
            }else{
                Session::flash('search_message', "No result found.");
            }
        }else{
            $pets = Pet::all();
        }
        
        //Get All categories
        $categories = Category::all();
        
        return view('pets.index', compact(['pets', 'categories']));
   }
}
