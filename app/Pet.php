<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Pet extends Eloquent
{
    protected $table = "pet"; 
    
    protected $fillable = ['category', 'name', 'photoUrls', 'tags', 'status'];
}
