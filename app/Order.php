<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Order extends Eloquent
{
    protected $table = "order"; 
}
