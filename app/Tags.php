<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Tags extends Eloquent
{
    protected $table = "tag"; 
}
